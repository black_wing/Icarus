var gulp        = require('gulp'); 
var browserSync = require('browser-sync');
var injector = require('bs-html-injector');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var postcss = require('gulp-postcss');
var plumber = require('gulp-plumber');
var lost = require('lost');
var injectSvg = require('gulp-inject-svg');
var injectSvgOptions = { base: '/src' };
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyJS = require('gulp-minify');
var rename = require("gulp-rename");
var pump = require('pump');
const imagemin = require('gulp-imagemin');
var svgmin = require('gulp-svgmin');
var csso = require('gulp-csso');
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var base64 = require('gulp-base64-inline');
var combineMq = require('gulp-combine-mq');
var merge = require('merge-stream');
var gcmq = require('gulp-group-css-media-queries');

var path = {
    build: { //Тут мы укаже куда складывать готовые после сборки файлы
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/css/',
        images: 'dist/images/',
        fonts: 'dist/fonts/',
        maps: 'maps/'
    },
    src: { //Пути откуда брать исходники
        pug: 'src/pug/pages/*', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/**/*.js',//В стилях и скриптах нам понадобятся только main файлы
        css: 'src/styles/css/**/*.css',
        scss: 'src/styles/scss/*.scss',
        images: 'src/images/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        pug: 'src/pug/**/*.pug',
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        css: 'src/styles/css/**/*.css',
        scss: 'src/styles/scss/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './dist'
};

gulp.task('browserSync', function() {
  browserSync.use(injector);
  browserSync({
    server: { baseDir: path.build.html },
    notify: true, 
    open: false,
    ghostMode: true,
  });
});
     
gulp.task('devStyles', function () {
    sassStream = 
        gulp.src(path.src.scss)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [
                'node_modules/susy/sass'
            ]
        })).on('error', sass.logError);

    cssStream = gulp.src(path.src.css).pipe(sourcemaps.init());

    merge(sassStream, cssStream)
        .pipe(csso())
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(postcss([
            lost()
        ]))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles.css'))
        .pipe(sourcemaps.write(path.build.maps))
        .pipe(gulp.dest(path.build.css));
});

gulp.task('buildStyles', function () {
    // styles
    var cssStream = gulp.src(path.src.css);
    var sassStream = gulp.src(path.src.scss);

    sassStream
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [
                'node_modules/susy/sass'
            ]
        })).on('error', sass.logError);

    merge(sassStream, cssStream)
        .pipe(csso())
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(postcss([
            lost()
        ]))
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(path.build.css));
});

gulp.task('pug', function buildHTML() {
  return gulp.src(path.src.pug)
  .pipe(plumber())
  .pipe(pug({
    pretty: false
  }))
  .pipe(injectSvg(injectSvgOptions))
  .pipe(htmlmin({collapseWhitespace: true}))
  .pipe(gulp.dest(path.build.html));
  gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
  // .pipe(reload({stream: true}));
});

gulp.task('scripts', function () {
    gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/slick-carousel/slick/slick.min.js',
        'node_modules/ion-rangeslider/js/ion.rangeSlider.min.js',
        'src/js/lib/*.js',
        path.src.js,
    ])
    .pipe(sourcemaps.init({loadMap: true, identityMap: true}))
    .pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write(path.build.maps))
    .pipe(gulp.dest(path.build.js))
});

gulp.task('watch',function(){    
    //livereload.listen();
    gulp.watch([path.watch.scss, path.watch.css], function () {
        setTimeout(function () {
            gulp.start('devStyles');
        }, 100);
    });
    gulp.watch(path.watch.pug, function () {
        setTimeout(function () {
            gulp.start('pug');
        }, 100);
    });
    gulp.watch(path.watch.js, function () {
        setTimeout(function () {
            gulp.start('scripts');
        }, 100);
    });
   // gulp.watch('css/*.css'); //.on('change', livereload.changed);
});

gulp.task('images', function buildHTML() {
  return  gulp.src('src/files/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/files/images'))
});

gulp.task('svg', function buildHTML() {
    return gulp.src('src/files/svg/*')
        .pipe(svgmin({
            plugins: [
            {
                removeDoctype: false
            }, 
            {
                removeComments: true
            }, 
            {
                removeTitle: true
            },
            {
                cleanupNumericValues: 
                {
                    floatPrecision: 2
                }
            }, 
            {
                convertColors: 
                {
                    names2hex: false,
                    rgb2hex: false
                }
            }]
        }))
        .pipe(gulp.dest('src/files/svg'));
});

gulp.task('build', ['buildStyles', 'svg', 'images', 'pug', 'scripts']);

gulp.task('run', ['browserSync', 'devStyles', 'watch', 'pug', 'scripts']);

gulp.task('default', ['run']);